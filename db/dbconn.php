<?php
    $charset = 'utf8mb4';
    
    $db_host = "localhost";
    $db_name = "event_booking";
    $db_user = "root";
    $db_pass = "root";

    try{
        $pdo = new PDO("mysql:host=" . $db_host . ";dbname=" . $db_name . ";charset=$charset", $db_user, $db_pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "<h1 class='text-success'>Database Connected! </h1>";
    } catch (PDOException $e) {
        // echo "<h1 class='text-danger'>No Database Found </h1>";
        // throw new PDOException($e->getMessage());
        $myObj = array(
            'status' => 500,
            'message' => "DB Connection ERROR"  
        );
        $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
        echo $myJSON;
    }
?>
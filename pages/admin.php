<?php 
session_start();

if(!isset($_SESSION["username"])){
    header("Location: ../index.php");
    exit();
} else if (isset($_SESSION["user_type"]) && $_SESSION["user_type"] == "User") {
    header("Location: user.php");
    exit();
}

$level ="..";
$title = "Admin Page";
// HTML STARTS BELOW
require_once dirname(__FILE__).'/../components/head-meta.php'; 
?>
<!-- custom CSS & JS below -->
<link rel="stylesheet" href="../css/admin.css">
<link rel="stylesheet" href="../css/events.css">
<script type="text/javascript" src="../js/admin.js"></script>
<!-- custom CSS & JS above -->
</head>
 <body>  
    <!-- === Page Content Goes Here below here === -->
    <!-- Modal for UPDATE-->
    <div class="modal fade" id="updateEventModal" tabindex="-1" role="dialog" aria-labelledby="updateEventModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
         <div class="modal-content">

            <div class="modal-header">
              <h5 class="modal-title" id="updateEventModalLabel">Update Event</h5>
              <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="font-size:1.5em">&times;</span>
              </button>
            </div>
            <div class="container mb-2">
                <form id="update-event" method="post" enctype="multipart/form-data" onsubmit="updateEventSubmit(event)">
                    <input type="hidden" id="eventID" name="eventID" value="">
                    <input type="hidden" id="currentEventFileName" name="currentEventFileName" value="">
                    <div class="form-group d-flex align-items-center mt-3 create-event">
                    <label for="event-name-update" class="mr-3" >Event Name</label>
                        <input id="event-name-field-update" type="text" class="form-control" name="event-name-field-update"  required="required">
                    </div>
                    <div class="custom-file">
                        <label id="custom-file-label-1" class="custom-file-label" for="file-update">Choose file</label>
                        <input id="event-file-field-update" type="file" name="file-update" class="custom-file-input" accept="image/*">
                    </div>
                                
                    <div class="modal-footer justify-content-center align-items-center flex-1 flex-row mt-4">
                        <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">Close</button>
                        <button type="submit" form="update-event" class="btn btn-primary">Save changes</button>
                    </div> 
                </form>
            </div>
          </div>
        </div>
      </div>

    <!-- Modal Delete Contact Confirmation-->
    <div class="modal fade" id="closeModal" tabindex="-1" role="dialog" aria-labelledby="closeModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="closeModalTitle">Are you sure?</h5>
              <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="font-size:1.5em">&times;</span>
              </button>
            </div>
            <div class="modal-footer d-flex justify-content-around">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No, keep Event</button>
              <form id="deleteEventForm" type="POST" onsubmit="deleteEventSubmit(event)" name="deleteEventForm">
                <input type="hidden" id="storeEventToDelete" name="storeEventToDelete">
                <button type="submit" class="btn btn-danger" id="delEventBtn" >Yes, Delete Event</button>
              </form>
            </div>
          </div>
        </div>
      </div>

    <div class="container d-flex flex-column align-items-center mt-5">
        <div class="d-flex flex-column align-items-center">
            <h3><?php
                echo isset($_SESSION["name"]) ? $_SESSION["name"] : "Hello Admin!";
            ?></h3>
            <div>
                <button type="button" class="btn btn-outline-secondary d-block" onclick="signoutClick(event)">Logout</button>
            </div>
        </div>
        <div class="w-100">
            <hr/>
        </div>
        <div class="d-flex">
            <button id="btn-event" class="btn btn-outline-secondary btn-sm" disabled >Create Event</button>
            <button id="btn-user" class="btn btn-outline-secondary btn-sm ml-3">Create User</button>
        </div>
        <div class="mt-3 w-100">
            <div class="w-100 d-flex flex-column align-items-center">
                <h3 id="title">Create Event</h3>
                <div class="w-100 d-flex flex-column align-items-center" >
                    <!-- CREATE EVENT FORM CONTENTS -->
                    <form id="create-event" method="post" enctype="multipart/form-data" onsubmit="createEventSubmit(event)">
                        <div class="form-group d-flex align-items-center mt-3 create-event">
                        <label for="event-name" class="mr-3" >Event Name</label>
                            <input id="event-name-field" type="text" class="form-control" name="event-name"  required="required">
                        </div>
                        <div class="custom-file">
                            <label id="custom-file-label-2" class="custom-file-label" for="customFile">Choose file</label>
                            <input id="event-file-field" type="file" name="file" class="custom-file-input" accept="image/*">
                        </div>
                    </form>
                    <!-- CREATE USER FORM CONTENTS -->
                    <form id="create-user" method="post" onsubmit="createUserSubmit(event)">
                        <div class="form-group d-flex align-items-center mt-3 create-user">
                            <label for="username" class="mr-3" >Username</label>
                            <input id="uname-field" type="text" class="form-control" name="username"  required="required">
                        </div>
                        <div class="form-group d-flex align-items-center create-user">
                            <label for="password" class="mr-3" >Password</label>
                            <input id="pass-field" type="password" class="form-control" name="password" required="required">
                        </div>
                        <div class="form-group d-flex align-items-center create-user">
                            <label for="name" class="mr-3" >Name</label>
                            <input id="name-field" type="text" class="form-control" name="name" required="required">
                        </div>
                        <div class="form-group d-flex align-items-center create-user">
                            <label for="email" class="mr-3" >Email</label>
                            <input id="email-field" type="email" class="form-control" name="email"  required="required">
                        </div>                        
                    </form>
                    <div class="w-100 mt-2">
                        <hr/>
                    </div>
                    <div>
                        <button type="submit" form="create-event" id="btn-submit" class="btn btn-outline-secondary mt-2">Submit</button>
                        <button type="submit" form="create-user" id="btn-register" class="btn btn-outline-secondary mt-2">Register</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="w-100 mt-3">
            <hr/>
        </div>
        <div class="d-flex justify-content-center flex-row mb-3">
            <button id="btn-load-events" class="btn btn-outline-secondary d-block">Display Events</button>
        </div>
        <div class="d-flex justify-content-center mb-5">
            <div id="events-display">

            </div>
        </div>
    </div>
    <!-- === Page Content Goes Here above here === -->
        </div>
<?php require_once dirname(__FILE__).'/../components/foot-meta.php'; ?>
<!-- Custom JS Scripts Below -->
    <script>
        // update event modal handler
        const updateEventHandler = (id) => {
            // // Grab DOM elements
            const cardTitle = document.getElementById('TITLE-'+id);
            const cardIMG = document.getElementById('IMG-'+id);
            const titleField = document.getElementById('event-name-field-update');
            const idField = document.getElementById('eventID');
            const imgFieldLabel = document.getElementById('custom-file-label-1');
            const imgFieldinput = document.getElementById('currentEventFileName');

            // // Populate the DOM elements with the data
            titleField.value = cardTitle.innerText;
            idField.value = id;
            imgFieldinput.value = cardIMG.getAttribute('name')
            imgFieldLabel.innerText = "Upload New (optional)";
        }

        const deleteEventHandler = (id) => {
            // Grab DOM elements
            const idDeleteField = document.getElementById('storeEventToDelete');
            idDeleteField.value = id;
        }

        // load events
        $(document).ready(function(){
            var url = window.location.href;
            var lastPart = url.substr(url.lastIndexOf('?') + 1);
            var scrollpos = url.split("?")[1];
            if (lastPart === "refresh") {
                $("#events-display").load("../php/get_all_events.php");
                setTimeout(function () {
                    window.scrollTo(0, scrollpos);
                },200);
            }

            $("#btn-load-events").click(function(){
                $("#events-display").load("../php/get_all_events.php");
            });

            // Load image everytime
            jQuery('img').each(function(){
                jQuery(this).attr('src',jQuery(this).attr('src')+ '?' + (new Date()).getTime());
            });
        });

        // fields
        const eventName = document.getElementById("event-name-field");
        const file = document.getElementById("event-file-field");
        const username = document.getElementById("uname-field");
        const password = document.getElementById("pass-field");
        const nameField = document.getElementById("name-field");
        const email = document.getElementById("email-field");

        // Forms
        const title =  document.getElementById("title");
        const eventForm = document.getElementById("create-event");
        const userForm = document.getElementById("create-user");
        const btn_event = document.getElementById("btn-event");
        const btn_user = document.getElementById("btn-user");
        const btn_submit =  document.getElementById("btn-submit");
        const btn_register =  document.getElementById("btn-register");

        username.setAttribute("disabled", true);
        password.setAttribute("disabled", true);
        nameField.setAttribute("disabled", true);
        email.setAttribute("disabled", true);
        btn_register.style.display = "none";

        btn_event.addEventListener("click",(e)=>{
            e.preventDefault();
            title.innerHTML  = "Create Event";
            btn_register.style.display = "none";
            btn_submit.style.display = "block";
            userForm.style.display = "none";
            eventForm.style.display = "block";

            btn_event.setAttribute("disabled", true);
            username.setAttribute("disabled", true);
            password.setAttribute("disabled", true);
            nameField.setAttribute("disabled", true);
            email.setAttribute("disabled", true);

            btn_user.removeAttribute("disabled");
            eventName.removeAttribute("disabled");
            file.removeAttribute("disabled");
        });

        btn_user.addEventListener("click",(e)=>{
            e.preventDefault();
            title.innerHTML  = "Create User";
            btn_register.style.display = "block";
            btn_submit.style.display = "none";
            userForm.style.display = "block";
            eventForm.style.display = "none";

            btn_event.removeAttribute("disabled");
            username.removeAttribute("disabled");
            password.removeAttribute("disabled");
            nameField.removeAttribute("disabled");
            email.removeAttribute("disabled");

            btn_user.setAttribute("disabled", true);
            eventName.setAttribute("disabled", true);
            file.setAttribute("disabled", true);
        });

        $('input[type="file"]').on('change',function(e){
                //get the file name
                var fileName = e.target.files[0].name;
                //replace the "Choose a file" label
                $('.custom-file-label').html(fileName);
        })


    </script>
</body>
</html>
<?php 
    $id = isset($id) ? $id : '0'; 
    $bookaction = "bookEventSubmit(event, ".$id.")";
    $cancellation = "cancelEventSubmit(event, ".$id.")";
    $type = isset($type) ? ($type == 'book' ? 'Book' : 'Cancel') : null;
    if( $type){
        $action = ($type == 'Book') ? $bookaction : $cancellation;
    }
?>
    <div id="<?php echo $id;?>" class="card shadow-sm mb-3 mb-lg-5" style="max-width: 60em; max-height: 60em;">
    <div class="mx-3 mt-3 image-card justify-content-center align-items-center" style="position:relative;min-height: 10em; background-color:#D3D3D3">
        <img id="<?php echo "IMG-".$id;?>" name="<?php echo $image_url;?>"class="img-fluid imgstyle" style="width: 100%; height:auto; position:absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);" 
            src="<?php echo isset($image_url) ? "../imguploads/".$image_url :"https://via.placeholder.com/300x250.png?text=No+Image+Provided";?>" 
            alt="Card image cap"
        >
    </div>
  
    <div class="card-body" style="min-height: 10em;">
        <h5 id="<?php echo "TITLE-".$id;?>" class="card-title text-center">
            <?php
                echo isset($card_title) ? $card_title : "No Event Title Provided";
            ?>
        </h5>

        <?php
             echo isset($_SESSION["user_type"]) ? 
             (
                $_SESSION["user_type"] == "Admin" ?
                "<div id='buttons' class='d-flex flex-column align-items-center justify-content-center'>
                    <button onclick='updateEventHandler(".$id.")' class='btn btn-outline-secondary mb-2' style='width:5em;' data-toggle='modal' data-target='#updateEventModal'>Update</button>
                    <button onclick='deleteEventHandler(".$id.")' class='btn btn-outline-secondary ' style='width:5em;'
                    data-toggle='modal' data-target='#closeModal'>Delete</button>
                 </div>"
                :
                (
                    $type ? 
                "<div class='d-flex flex-column align-items-center justify-content-center my-4'>
                <button  id='button-".$id."' type='button' onclick='(".$action.")' class='btn btn-outline-secondary btn-lg' style='width:5em;'>".$type."</button>
                </div>" : ""
                )
             ) 
             : 
                (
                    $type ? 
                "<div  class='d-flex flex-column align-items-center justify-content-center my-4'>
                <button id='button-".$id."' type='button' onclick='(".$action.")' class='btn btn-outline-secondary btn-lg' style='width:5em;'>".$type."</button>
                </div>" : ""
                )
             ;
        ?>

        <!-- <div id="buttons" class="d-flex flex-column align-items-center justify-content-center">
            <button class="btn btn-outline-secondary mb-2" style="width:5em;">Update</button>
            <button class="btn btn-outline-secondary " style="width:5em;">Delete</button>
        </div>

        <div id="buttons" class="d-flex flex-column align-items-center justify-content-center my-4">
            <button class="btn btn-outline-secondary" style="width:5em;">Book</button>
        </div> -->
  </div>
</div>
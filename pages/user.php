<?php 
session_start();
if(!isset($_SESSION["username"])){
    header("Location: ../index.php");
    exit();
} else if (isset($_SESSION["user_type"]) && $_SESSION["user_type"] == "Admin") {
    header("Location: admin.php");
    exit();
}

$level ="..";
$title = "User Page";
// HTML STARTS BELOW
require_once dirname(__FILE__).'/../components/head-meta.php'; 
?>
<!-- custom CSS below -->
<link rel="stylesheet" href="../css/user.css">
<link rel="stylesheet" href="../css/events.css">
<script type="text/javascript" src="../js/user.js"></script>
<!-- custom CSS above -->
</head>
 <body>  
    <!-- === Page Content Goes Here below here === -->
    <div name="<?php echo $_SESSION['id']?>" id="user-id-ref"></div>
    <div class="container d-flex flex-column align-items-center mt-5 mb-5">
        <div class="d-flex flex-column align-items-center">
            <h3><?php
                echo isset($_SESSION["name"]) ? $_SESSION["name"] : "Hello User!";
            ?></h3>
            <div>
                <button type="button" class="btn btn-outline-secondary d-block btn-sm" onclick="signoutClick(event)">Logout</button>
            </div>
        </div>
        <div class="w-100">
            <hr/>
        </div>
        <div class="w-100">
            <hr/>
        </div>
        <div class="d-flex">
            <button id="btn-event" class="btn btn-outline-secondary " disabled >Display Event</button>
            <button id="btn-booked" class="btn btn-outline-secondary ml-3">Booked Events</button>
        </div>
        <div class="mt-3 w-100">
            <div class="w-100 d-flex flex-column align-items-center">
            <!-- SHOW EVENTS HERE - BOOKED & NON BOOKED -->
                <div  class="container"><h3 id="show-title" class="text-center mb-3">Available Events</h3></div>
                <div id="show-event">
                    
                </div>
                <div id="show-booked">

                </div>
            </div>
        </div>
    </div>
  
    <!-- === Page Content Goes Here above here === -->
        </div>
<?php require_once dirname(__FILE__).'/../components/foot-meta.php'; ?>
<!-- Custom JS Scripts Below -->
    <script>
        var btn_event = document.getElementById("btn-event");
        var btn_booked = document.getElementById("btn-booked");
        var show_event = document.getElementById("show-event");
        var show_booked = document.getElementById("show-booked");
        var user_id = document.getElementById("user-id-ref").getAttribute('name');
        var title = document.getElementById("show-title");

        btn_event.addEventListener("click",()=>{
            show_booked.style.display = "none";
            show_event.style.display = "block";
            btn_event.setAttribute("disabled", true);
            btn_booked.removeAttribute("disabled");
            title.innerText = "Available Events";
        });

        btn_booked.addEventListener("click",()=>{
            show_booked.style.display = "block";
            show_event.style.display = "none";
            btn_event.removeAttribute("disabled");
            btn_booked.setAttribute("disabled", true);
            title.innerText = "Your Booked Events";
        });

        // load events
        $(document).ready(function(){
            var url = window.location.href;
            var lastPart = url.substr(url.lastIndexOf('?') + 1);
            if (lastPart === "cancelled") {
                show_booked.style.display = "block";
                show_event.style.display = "none";
                btn_event.removeAttribute("disabled");
                btn_booked.setAttribute("disabled", true);
                title.innerText = "Your Booked Events";
                $("#show-booked").load("../php/get_user_bookings.php",{
                    id: user_id
                });
            } else {
                $("#show-event").load("../php/get_user_events.php",{
                    id: user_id
                });
            }


            $("#btn-event").click(function(){
                $("#show-event").load("../php/get_user_events.php",{
                    id: user_id
                });
            });

            $("#btn-booked").click(function(){
                $("#show-booked").load("../php/get_user_bookings.php",{
                    id: user_id
                });
            });

            // $("#btn-booked").click(()=>{$("#show-booked").load("../php/get_user_bookings.php",{
            //     id: $_SESSION['id']
            // }););
        });

    </script>
</body>
</html>
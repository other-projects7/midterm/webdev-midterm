<?php
    session_start();
    if(isset($_SESSION['username'])){
        isset($_SESSION["user_type"]) && $_SESSION["user_type"] == "admin" 
        ? header("Location: ./pages/admin.php")
        : header("Location: ./pages/user.php");
        exit();
    }

$level =".";
$title = "Login Page";
// HTML STARTS BELOW
require_once dirname(__FILE__).'/components/head-meta.php'; 
?>
<!-- custom CSS below -->
<link rel="stylesheet" href="./css/index.css">
<!-- custom CSS above -->
</head>
 <body>  
    <!-- === Content Goes Here below here === -->
    <div class="container d-flex justify-content-center align-items-center w-100 vh-100" style="max-width:50em;">
        <div class="card">
            <form id="signinForm" class="card-body" method="POST">
                <h2>LOG IN</h2>
                <div class="form-group d-flex align-items-center mt-3">
                    <label for="username" class="mr-3" >Username</label>
                    <input type="text" class="form-control" name="username" id="username" required="required">
                </div>
                <div class="form-group d-flex align-items-center">
                    <label for="password" class="mr-3" >Password</label>
                    <input type="password" class="form-control" name="password" id="password" required="required">
                </div>
                <button id="btnsubmit" onclick="signinSubmit(event)" type="button" name="btnsubmit" class="btn btn-secondary btn-lg w-100 mt-3" value="submit">Submit</button>
            </form>
        </div>
    </div>

    <!-- === Custom Page Content Goes Here above here === -->
    </div>
    <?php require_once dirname(__FILE__).'/components/foot-meta.php'; ?>
    <!-- Custom JS Scripts Below -->
    <script>
          
    </script>
</body>
</html>
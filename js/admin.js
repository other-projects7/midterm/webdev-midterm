function createUserSubmit(e){
    e.preventDefault();
    var url = "../php/create_user.php";
    var data = $("#create-user").serialize();
    var urlData = url+"?"+data;
    xhttp.open("GET", urlData, true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var res = JSON.parse(this.responseText);
            if(res["status"] == 200){
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: "User Sucessfully Registered!",
                    confirmButtonText: 'Close'
                }).then(result => {
                    if (result.isConfirmed) {
                        window.location.replace('../pages/admin.php');
                    }
                });
                $('#create-user')[0].reset();
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res["message"],
                });
            }
        }
    };
}


function createEventSubmit(e){
    e.preventDefault();

    // Grab the form HTML DOM element
    let myForm = $("#create-event")[0];

    // Place All the data in a new form data
    let formData = new FormData(myForm);

    // Grab the uploaded images
    let imageFiles = $('#event-file-field')[0].files;

    // Validation # 1 - User must upload at least 1 image
    let totalImages = imageFiles.length;

    // Validation # 2 - User must upload images below 1000000
    let fileSize = imageFiles[0].size;

    if(totalImages < 1 ){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please upload an image for your event.',
        });
    } else if (fileSize > 1000000){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Your file size is too large! Please upload a file below 1MB.',
        });
    } else {
        
        formData.append(`image`, imageFiles[0]);

        $.ajax({
            type : 'POST',
            url : '../php/create_event.php',
            data : formData,
            contentType: false,
            processData: false,
            success : function(response) {
                var res = JSON.parse(response);
                setTimeout(function() {
                    if(res["status"] == 200){
                        Swal.fire({
                            icon: 'success',
                            title: 'Event Created',
                            text: "Your event has been created successfully!",
                            confirmButtonText: 'Close'
                        }).then(result => {
                            if (result.isConfirmed) {
                                window.location.replace('../pages/admin.php');
                            }
                        });
                        myForm.reset();
                        $('#custom-file-label-2').html('');
      
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: res["message"],
                        });
                    }
                },100); 
            },
            error: function(xhr, status, error) {
                setTimeout(function() {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong. Please try again...',
                    });
                },100);
            }
        });
    }
}


function updateEventSubmit(e){
    e.preventDefault();
    // Grab the form HTML DOM element
    let myForm = $("#update-event")[0];

    // Place All the data in a new form data
    let formData = new FormData(myForm);

    // Grab the uploaded images
    let imageFiles = $('#event-file-field-update')[0].files;

    // check filesize if there is a file
     if (imageFiles && imageFiles[0]?.size > 1000000){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Your file size is too large! Please upload a file below 1MB.',
        });
    } else {

        // Append new uploaded file to form data, otherwise append none
        if(imageFiles?.length>=1){
            formData.append(`image`, imageFiles[0]);
        } else {
            formData.append(`image`, "NONE");
        }

        console.log(imageFiles);
        
        $.ajax({
            type : 'POST',
            url : '../php/update_event.php',
            data : formData,
            contentType: false,
            processData: false,
            success : function(response) {
                var res = JSON.parse(response);
                setTimeout(function() {
                    if(res["status"] == 200){
                        Swal.fire({
                            icon: 'success',
                            title: 'Event Updated',
                            text: res["message"],
                            confirmButtonText: 'Close'
                        }).then(result => {
                            $('#updateEventModal').modal('hide');
                            window.location.replace('../pages/admin.php?'+document.documentElement.scrollTop+'?refresh');
                        });
                        myForm.reset();
                        $('#custom-file-label-1').html('');
                        $(res["data"]).attr('src?' + new Date().getTime());
                        
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: res["message"],
                        });
                    }
                },100); 
            },
            error: function(xhr, status, error) {
                setTimeout(function() {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong. Please try again...',
                    });
                },100);
            }
        });
    }
}

function deleteEventSubmit(e){
    e.preventDefault();
    console.log('Delete event');
    var url = "../php/delete_event.php";
    var data = $("#deleteEventForm").serialize();
    var urlData = url+"?"+data;
    xhttp.open("GET", urlData, true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var res = JSON.parse(this.responseText);
            if(res["status"] == 200){
                $('#deleteEventForm')[0].reset();
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: res["message"],
                    confirmButtonText: 'Close'
                }).then(result => {
                    $('#closeModal').modal('hide');
                    window.location.replace('../pages/admin.php?'+document.documentElement.scrollTop+'?refresh');
                });
                // window.location.replace('../pages/admin.php');
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res["message"],
                });
            }
        }
    };
}

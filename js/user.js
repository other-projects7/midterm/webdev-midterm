function bookEventSubmit(e, eid){
    e.preventDefault();
    var url = "../php/book_event.php";
    var user_id = document.getElementById("user-id-ref").getAttribute('name');
    var urlData = url+"?id="+user_id+"&eid="+eid;
    xhttp.open("GET", urlData, true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var res = JSON.parse(this.responseText);
            if(res["status"] == 200){
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: "User Sucessfully Registered!",
                    confirmButtonText: 'Close'
                }).then(result => {
                    window.location.replace('../pages/user.php');
                });
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res["message"],
                });
            }
        }
    };
}

function cancelEventSubmit(e, eid){
    e.preventDefault();
    var url = "../php/cancel_event.php";
    var user_id = document.getElementById("user-id-ref").getAttribute('name');
    var urlData = url+"?id="+user_id+"&eid="+eid;
    xhttp.open("GET", urlData, true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var res = JSON.parse(this.responseText);
            if(res["status"] == 200){
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: res["message"],
                    confirmButtonText: 'Close'
                }).then(result => {
                    window.location.replace('../pages/user.php?cancelled');
                });
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res["message"],
                });
            }
        }
    };
}
var xhttp = new XMLHttpRequest();

function signinSubmit(e){
    e.preventDefault();
    var url = "../midterm/php/signin_action.php";
    var data = $("#signinForm").serialize();
    var urlData = url+"?"+data;
    xhttp.open("GET", urlData, true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var res = JSON.parse(this.responseText);
            if(res["status"] == 200){
                $('#signinForm')[0].reset();
                window.location.replace('../midterm/pages/admin.php');
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res["message"],
                  })
            }
        }
    };
}

function signoutClick(e){
    var url = "../php/signout_action.php";
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var res = JSON.parse(this.responseText);
            if(res["status"] == 200){
                window.location.replace('../index.php');
            }
        }
    };
}


<?php
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    session_start();
    $retVal = "";
    $isValid = true;
    $status = 400;

    // CHECK ISSET, CHECK IS EMPTY
    $eventName = isset($_REQUEST['event-name']) ? trim($_REQUEST['event-name']) : null;

    $file =  $_FILES['file'];
    $fileName =  $file['name'];
    $fileTmpName = $file['tmp_name'];
    $fileSize = $file['size'];
    $fileError = $file['error'];
    $fileType = $file['type'];
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed = array('jpg', 'jpeg', 'png');

    // Check fields are empty or not
    if(!$eventName || $eventName == ''|| empty($eventName)){
        $isValid = false;
        $retVal = "Please fill all fields.";
    }

    // Check file extension
    if($isValid && !in_array($fileActualExt,$allowed)){
        $retVal = "You cannot upload files of this type. Please upload jpg or png images only.";
        $isValid = false;
    }

    // Check if there were errors on the file upload
    if($isValid && $fileError){
        $retVal = "There was an error uploading the file. Please try again";
        $isValid = false;
    }

    // Check file size
    if($isValid && $fileSize > 1000000){
        $retVal = "Your file size is too large! Please upload a file below 1MB.";
        $isValid = false;
    }

    // Upload the file
    if($isValid){
        $fileNameNew = uniqid('', true).".".$fileActualExt;
        $fileDestination = dirname(__FILE__).'/../imguploads/'.$fileNameNew;
        move_uploaded_file($fileTmpName, $fileDestination);
    }

    // Save into DB
    if($isValid){
        try{
            $insertSQL = "INSERT INTO events(Event_name,event_image) 
            values(:name,:image)";

            $query = $pdo->prepare($insertSQL);

            $query->bindParam(':name', $eventName);
            $query->bindParam(':image', $fileNameNew);
            $query->execute();

            // close the DB connection
            $pdo = null;
    
            $retVal = "Event added successfully.";
            $status = 200;
        } catch(PDOException $e){
            $retVal = $e->getMessage();
        }
    }

    $myObj = array(
        'status' => $status,
        'message' => $retVal,
    );

    $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    echo $myJSON;

?>
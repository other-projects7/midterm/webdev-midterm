<?php
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    session_start();
    $retVal = "";
    $isValid = true;
    $status = 400;

    // CHECK ISSET, CHECK IS EMPTY
    $username = isset($_REQUEST['username']) ? strtolower(trim($_REQUEST['username'])) : null;
    $password = isset($_REQUEST['password']) ? strtolower(trim($_REQUEST['password'])) : null;
    $name = isset($_REQUEST['name']) ? trim($_REQUEST['name']) : null;
    $email = isset($_REQUEST['email']) ? trim($_REQUEST['email']) : null;

    // Check fields are empty or not
    if(!$username || !$password || !$name || !$email ||
        $username == '' || $password == '' || empty($username) || empty($password) ||
        $name == '' || $name == '' || empty($email) || empty($email)
    ){
        $isValid = false;
        $retVal = "Please fill all fields.";
    }

    // Check if username already exists
    if($isValid){
        $sql="SELECT * FROM users_details WHERE username = :username";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['username' => $username]); 
        $user = $stmt->fetch();

        $isValid = $user ? false : true;
        $retVal = "Username already exists";
    }

    // Check if email is valid or not
    if ($isValid && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $isValid = false;
        $retVal = "Invalid email.";
    }

    // Check if email already exists and if valid email
    if($isValid){
        $sql="SELECT * FROM users_details WHERE email = :email";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['email' => $email]); 
        $user = $stmt->fetch();

        $isValid = $user ? false : true;
        $retVal = "Email already exists";
    }

    // Save into DB
    if($isValid){
        try{
            // Hash password
            $password = password_hash($password, PASSWORD_DEFAULT);

            $insertSQL = "INSERT INTO users_details(username,password,name,email) 
            values(:username,:password,:name,:email)";

            $query = $pdo->prepare($insertSQL);

            $query->bindParam(':username', $username);
            $query->bindParam(':password', $password);
            $query->bindParam(':name', $name);
            $query->bindParam(':email', $email);
            $query->execute();

            // close the DB connection
            $pdo = null;
    
            $retVal = "Contact added successfully.";
            $status = 200;
        } catch(PDOException $e){
            $retVal = $e->getMessage();
        }
    }

    $myObj = array(
        'status' => $status,
        'message' => $retVal
    );

    $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    echo $myJSON;
?>
<?php
/// MODIFY
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    session_start();
    $retVal = "";
    $isValid = true;
    $status = 400;

    // CHECK ISSET, CHECK IS EMPTY
    $id = isset($_REQUEST['id']) ? strtolower(trim($_REQUEST['id'])) : null;
    $eid = isset($_REQUEST['eid']) ? strtolower(trim($_REQUEST['eid'])) : null;

    // Check if ID is set
    if(!$id){
        $isValid = false;
        $retVal =  "<h3>User not found! Please try logging in again<h3>";
    } 

    // Check if user already saved a booking under event
    if($isValid){
        $sql="SELECT * FROM `bookings` WHERE `event_id`=:EID AND `user_id`=:UsrID";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':UsrID', $id);
        $stmt->bindParam(':EID', $eid);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if($result){
            $isValid = false;
            $retVal =  "User Already booked event! Please refresh your page.";  
        }
    }

    // SAVE BOOKING
    if($isValid){
        try{
            $insertSQL = "INSERT INTO bookings(`event_id`,`user_id`) VALUES(:EID,:UsrID)";
            $stmt = $pdo->prepare($insertSQL);
            $stmt->bindParam(':UsrID', $id);
            $stmt->bindParam(':EID', $eid);
            $stmt->execute();

            // close the DB connection
            $pdo = null;
            $retVal = "This event is added to your bookings!";
            $status = 200;
        } catch(PDOException $e){
            $retVal = $e->getMessage();
        }
    }

    $myObj = array(
        'status' => $status,
        'message' => $retVal
    );

    $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    echo $myJSON;

?>
<?php
// modify
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    session_start();
    $retVal = "";
    $isValid = true;
    $status = 400;

    // CHECK ISSET, CHECK IS EMPTY
    $eventID = isset($_REQUEST['eventID']) ? strtolower(trim($_REQUEST['eventID'])) : null;
    $currentEventFileName = isset($_REQUEST['currentEventFileName']) ? strtolower(trim($_REQUEST['currentEventFileName'])) : null;
    $eventName = isset($_REQUEST['event-name-field-update']) ? trim($_REQUEST['event-name-field-update']) : null;
    $file =  isset($_FILES['file-update']) ? $_FILES['file-update'] : null;

    // Check fields were successfully mounted
    if(!$eventID || $eventID == ''|| empty($eventID) || !$currentEventFileName || $currentEventFileName == ''|| empty($currentEventFileName )){
        $isValid = false;
        $retVal = "An error occured with the upload. Please try again...";
    }

    // Check file properties and overwrite existing file
    if($isValid && $file){
        $fileName =  isset($file['name']) ? $file['name'] : null;
        $fileTmpName = isset($file['tmp_name']) ? $file['tmp_name'] : null;
        $fileSize = isset($file['size']) ? $file['size'] : null;
        $fileError = isset($file['error']) ? $file['error'] : null;
        $fileType = isset($file['type']) ? $file['type'] : null;
        $fileExt = isset($fileName) ? explode('.', $fileName)  : null;
        $fileActualExt = isset($fileExt) ? strtolower(end($fileExt))  : null;
        $allowed = array('jpg', 'jpeg', 'png');

        // Check file extension
        if($isValid && $fileSize && !in_array($fileActualExt,$allowed)){
            $retVal = "You cannot upload files of this type. Please upload jpg or png images only.";
            $isValid = false;
        }

        // Check if there were errors on the file upload
        if($isValid && $fileSize && $fileError){
            $retVal = "There was an error uploading the file. Please try again";
            $isValid = false;
        }

        // Check file size
        if($isValid && $fileSize > 1000000){
            if($fileSize == 0 || $fileSize == null){
                $retVal = "An error occured with the file upload. Please try again.";
            } else {
                $retVal = "Your file size is too large! Please upload a file below 1MB.";
            }
            $isValid = false;
        }

        // Overwrite existing file
        if($isValid && $fileTmpName){
            $fileDestination = dirname(__FILE__).'/../imguploads/'.$currentEventFileName;
            move_uploaded_file($fileTmpName, $fileDestination);
        }
    } 

    // Update DB if file name is different
    if($isValid){
        try{
            // Check if name already matches in the database
            $checkSQL = "SELECT * FROM `events` WHERE Event_id = :id";
            $query = $pdo->prepare($checkSQL);
            $query->bindParam(':id', $eventID);
            $query->execute();
            $result = $query->fetchAll();

            if($result[0]['Event_name'] != $eventName){
                $updateSQL = "UPDATE `events` SET `Event_name` = :newntitle WHERE `events`.`Event_id` = :id";
                $query = $pdo->prepare($updateSQL);
                $query->bindParam(':id', $eventID);
                $query->bindParam(':newntitle', $eventName);
                $query->execute();

                // close the DB connection
                $pdo = null;

                $retVal = 'File successfully saved!';
            } 

            $status = 200;
   
        }catch(PDOException $e){
            $retVal = $e->getMessage();
        }
        
    }

    $myObj = array(
        'status' => $status,
        'message' => $retVal,
        'data' => 'IMG-'.$eventID,
        'filepath' => "../imguploads/".$currentEventFileName
    );

    $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    echo $myJSON;

?>
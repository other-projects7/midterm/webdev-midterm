<?php
/// MODIFY
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    session_start();
    $retVal = "";
    $isValid = true;
    $status = 400;
      
    // CHECK ISSET, CHECK IS EMPTY
    $id = isset($_REQUEST['storeEventToDelete']) ? strtolower(trim($_REQUEST['storeEventToDelete'])) : null;

    // Check fields are empty or not
    if(!$id || $id == ''|| empty($id)){
        $isValid = false;
        $retVal = "Error Deleting item. Please try again.";
    }

    // Check if ID exhists in the table
    if($isValid){
        try{
            $sql="SELECT * FROM `events` WHERE Event_id=:id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll();

            // DELET FILE IN FOLDER
            if($result){
                $filename = $result[0][2];
                $retVal = $filename;
                $filename = "../imguploads/".$filename;

                try{
                    if(!unlink($filename)){
                        $isValid = false;
                        $retVal = "Error Deleting item. Please try again.";
                    }
                }catch(Exception $e){
                    $retVal = $retVal.$e->getMessage();
                }

            }
            else{
                $isValid = false;
                $retVal = "Event not found! Please refresh the pagea and try again!";
            }
        } catch (PDOException $e){
            $retVal = $e->getMessage();
        }
    }

    // Save into DB
    if($isValid){
        try{
            $deletesql = "DELETE FROM events WHERE Event_id=:id";

            $query = $pdo->prepare($deletesql);
            $query->bindParam(':id', $id);

            $query->execute();

            // close the DB connection
            $pdo = null;
    
            $retVal = "Event deleted successfully.";
            $status = 200;
        } catch(PDOException $e){
            $retVal = $e->getMessage();
        }
    }


    $myObj = array(
        'status' => $status,
        'message' => $retVal
    );

    $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    echo $myJSON;

?>
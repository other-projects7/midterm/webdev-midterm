<?php
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    // session_start();
    // $retVal = "";
    $isValid = true;
    // $status = 400;

    // CHECK ISSET, CHECK IS EMPTY
    $id = isset($_REQUEST['id']) ? strtolower(trim($_REQUEST['id'])) : null;

    if(!$id){
        $isValid = false;
        echo "<h3>User not found! Please try logging in again<h3>";
    } 

    // GET EVENTS
    if($isValid){
        try{
            $sql="  SELECT * 
            FROM `events` 
            LEFT JOIN bookings ON events.Event_id = bookings.event_id
            WHERE user_id is null  or  (user_id != :id and  events.Event_id NOT IN(
                SELECT events.Event_id as tables
                FROM `events` 
                LEFT JOIN bookings ON events.Event_id = bookings.event_id
                WHERE user_id = :id
            ) ) GROUP BY events.Event_id ";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll();
            // close the DB connection
            $pdo = null;
            if($result){
                foreach ($result as &$event) {
                    $type = 'book';
                    $image_url = $event['event_image'];
                    $card_title = $event['Event_name'];
                    $id = $event['Event_id'];
                    include dirname(__FILE__).'/../pages/events.php';
                }
            } else {
                echo "<h3>There are no events to display!<h3>";
            }
        } catch(PDOException $e){
            $retVal = $e->getMessage();
        }
    }

    // $myObj = array(
    //     'status' => $status,
    //     'message' => $retVal
    // );

    // $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    // echo $myJSON;
?>
<?php
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    session_start();
    $retVal = "";
    $isValid = true;
    $status = 400;

    // CHECK ISSET, CHECK IS EMPTY
    $username = isset($_REQUEST['username']) ? trim($_REQUEST['username']) : null;
    $password = isset($_REQUEST['password']) ? trim($_REQUEST['password']) : null;


    // Check fields are empty or not
    if( !$username || !$password || $username == '' || $password == '' || empty($username) || empty($password)){
        $isValid = false;
        $retVal = "Please fill all fields.";
    }

    // Sign-in actions
    if($isValid){
        // Check if username already exists
        $sql="SELECT * FROM users_details WHERE username = :username";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['username' => $username]); 
        $user = $stmt->fetch();
        // close the DB connection
        $pdo = null;
 
        //Check if user exists
        if($user){
            // Verify Password
            $isPassword = password_verify ($password , $user["password"]);
            if($isPassword == true){
                $retVal = "Success";
                $status = 200;
                $_SESSION['username'] = $user["username"];
                $_SESSION['user_type'] = $user["user_type"];
                $_SESSION['name'] = $user["name"];
                $_SESSION['id'] = $user["user_id"];
            } else {
                $retVal = "Wrong Password entered";
            }
        } else {
            $retVal = "Account Does not Exist";
        }
    }

    $myObj = array(
        'status' => $status,
        'message' => $retVal
    );

    $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    echo $myJSON;
?>
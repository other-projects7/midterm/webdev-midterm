<?php
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    // session_start();
    $retVal_b = "";
    $isValid_b = true;
    // $status = 400;

    // CHECK ISSET, CHECK IS EMPTY
    $id = isset($_REQUEST['id']) ? strtolower(trim($_REQUEST['id'])) : null;

    if(!$id){
        $isValid = false;
        echo "<h3>User not found! Please try logging in again<h3>";
    } 

    // GET EVENTS
    if($isValid_b){
        try{

            $sql=" SELECT * FROM `events` 
            LEFT JOIN bookings ON events.Event_id = bookings.event_id
            WHERE user_id = :id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll();
            // close the DB connection
            $pdo = null;
            if($result){
                foreach ($result as &$event) {
                    $type = 'cancel';
                    $image_url = $event['event_image'];
                    $card_title = $event['Event_name'];
                    $id = $event['Event_id'];
                    include dirname(__FILE__).'/../pages/events.php';
                }
            } else {
                echo "<h3>You have no booked events!<h3>";
            }
        } catch(PDOException $e){
            $retVal_b = $e->getMessage();
        }
    }

    // $myObj = array(
    //     'status' => $status,
    //     'message' => $retVal
    // );

    // $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    // echo $myJSON;
?>
<?php
    require_once dirname(__FILE__).'/../db/dbconn.php'; 
    session_start();

    // GET EVENTS
    try{
        $sql="SELECT * FROM `events` ORDER BY `Event_id` DESC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        // close the DB connection
        $pdo = null;
        if($result){
            foreach ($result as &$event) {
                $image_url = $event['event_image'];
                $card_title = $event['Event_name'];
                $id = $event['Event_id'];
                include dirname(__FILE__).'/../pages/events.php';
            }
        } else {
            echo "<h3>There are no events to display!<h3>";
        }
    } catch(PDOException $e){
        $retVal = $e->getMessage();
    }
    
    // $myObj = array(
    //     'result' => $result
    // );

    // $myJSON = json_encode($myObj, JSON_FORCE_OBJECT);
    // echo $myJSON;

?>